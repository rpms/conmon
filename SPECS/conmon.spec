%global with_debug 0
%global with_check 0

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package %{nil}
%endif

%global provider github
%global provider_tld com
%global project containers
%global repo conmon
# https://github.com/containers/conmon
%global import_path %{provider}.%{provider_tld}/%{project}/%{repo}
%global commit0 1bddbf7051a973f4a4fecf06faa0c48e82f1e9e1
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global git0 https://%{import_path}

Name: %{repo}
Epoch: 2
Version: 2.0.15
Release: 1%{?dist}
Summary: OCI container runtime monitor
License: ASL 2.0
URL: %{git0}
Source0: %{git0}/archive/%{commit0}/%{name}-%{shortcommit0}.tar.gz
BuildRequires: gcc
BuildRequires: git
BuildRequires: glib2-devel
BuildRequires: systemd-devel

%description
%{summary}.

%prep
%autosetup -Sgit -n %{name}-%{commit0}

%build
%{__make} all

%install
%{__make} PREFIX=%{buildroot}%{_prefix} install

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Mon Apr 06 2020 Jindrich Novy <jnovy@redhat.com> - 2:2.0.15-1
- update to 2.0.15
- Related: #1821193

* Thu Dec 12 2019 Jindrich Novy <jnovy@redhat.com> - 2:2.0.6-1
- update to 2.0.6
- Related: RHELPLAN-25139

* Tue Dec 10 2019 Jindrich Novy <jnovy@redhat.com> - 2:2.0.5-1
- update to 2.0.5
- Related: RHELPLAN-25139

* Tue Dec 10 2019 Jindrich Novy <jnovy@redhat.com> - 2:2.0.4-1
- update to 2.0.4 bugfix release
- Related: RHELPLAN-25139

* Wed Nov 20 2019 Jindrich Novy <jnovy@redhat.com> - 2:2.0.3-1.giteb5fa88
- update to 2.0.3
- Related: RHELPLAN-25139

* Wed Sep 25 2019 Lokesh Mandvekar <lsm5@fedoraproject.org> - 2:2.0.2-0.1.dev.git422ce21
- build latest upstream master

* Tue Sep 10 2019 Lokesh Mandvekar <lsm5@fedoraproject.org> - 2:2.0.0-2
- remove BR: go-md2man since no manpages yet

* Tue Sep 10 2019 Lokesh Mandvekar <lsm5@fedoraproject.org> - 2:2.0.0-1
- bump to v2.0.0

* Fri May 31 2019 Lokesh Mandvekar <lsm5@fedoraproject.org> - 2:0.2.0-1
- initial package
